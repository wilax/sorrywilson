import React from 'react';
import flickr from '../api/flickr';
import SearchBar from './SearchBar';
import ImageList from './ImageList';
// import logo from './img/sw-logo.svg';
// import '../css/App.css';

class App extends React.Component {
    state = { images: [] };
    
  
  onSearchSubmit = async (images) => {
    const response = await flickr.get('https://api.flickr.com/services/rest/?method=flickr.photos.getRecent&api_key=c77106e8aeacd801918789f8dcf36e53e&per_page=10&format=json&nojsoncallback=1')
    this.setState({
      images: response.data.photos.photo,
    });
  
      this.setState({ images: response.data.photos });
    }

  render() {
    return (
      <div className="ui container" style={{ marginTop: "10px" }}>
        <SearchBar onSubmit={this.onSearchSubmit} />
        <ImageList images={this.state.images} />
      </div>
    );
  }
}


export default App;
